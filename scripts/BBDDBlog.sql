USE Blog;

CREATE TABLE USUARIO(
Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
Nombre Varchar(100) NOT NULL,
Apellidos varchar (100) NOT NULL,
Alias nvarchar(50) NOT NULL,
Email varchar(200) NOT NULL,
Telefono varchar(200) NOT NULL,
CodPostal varchar(100) NOT NULL,
Ciudad varchar(100) NOT NULL,
)

CREATE TABLE CATEGORIA(
Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
Nombre Varchar(50) NOT NULL,
)

CREATE TABLE POST(
Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
Id_Usuario int NOT NULL,
Fecha varchar(50) NOT NULL,
Titulo varchar (100) NOT NULL,
Contenido varchar (700) NOT NULL,
Id_Categoria int NOT NULL,
constraint ID_PostUsuario foreign key (Id_Usuario) references USUARIO(Id),
constraint ID_CategoriaUsuario foreign key (Id_Categoria) references CATEGORIA(Id)
)