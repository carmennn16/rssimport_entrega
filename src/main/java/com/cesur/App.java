package com.cesur;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.Scanner;


/**
 * Hello world!
 *
 */
public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="Blog";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    /** 
     * @param args
     */
    public static void main( String[] args )
    {
        CrearBBDD();
        crearMenu();

        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        /**  b.modificarBBDD("insert into clientes values ('bernat','bernat@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
        b.leerBBDD("select top 1  * from clientes order by id desc",7);
        String ret = b.leerBBDDUnDato("select max(id) from clientes");
        b.modificarBBDD("delete from clientes where id=" + ret);
        b.leerBBDD("select top 1  * from clientes order by id desc",7);
        XmlParse x =new XmlParse();
        String url ="https://sites.google.com/site/falfiles/Home/archivos/ejemplo.xml";
        List<String>  list = x.leer("Descargas/descarga/titulo/text()",url);
        System.out.println(("Total Libros: " + list.size()));
        list = x.leer("Descargas/descarga[@id='1']/titulo/text()",url);

        for (String string : list) {
            System.out.println(("Titulo con id=1: " + string));
        } */

        Scanner sc = new Scanner (System.in);



    }



    private static void CrearBBDD()
    {
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       // String cero = "0";
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/BBDDBlog.sql");
          
            b.modificarBBDD(query);
            b.modificarBBDD("insert into clientes values ('Angeles','angeles@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
      
        }
    }

    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }

    
    
    public static void crearMenu(){

    
       Scanner sn = new Scanner(System.in);
       boolean salir = false;
       int opcion;

       try {
        
       while(!salir){
            
           System.out.println("0. Importar RSS ");
           System.out.println("1. Ver ultimos 10 post");
           System.out.println("2. Ver todos los post");
           System.out.println("3. buscar por titulo o contenido una palabra (indicar la palabra por la que buscar)");
           System.out.println("4. Filtrar por categoria");
           System.out.println("5. Salir");
            
           System.out.println("Escribe una de las opciones");
           opcion = sn.nextInt();
            
           switch(opcion){
               case 0:
                   System.out.println("Has seleccionado la opcion 0, introduce una URL");
                   importarRSS();
                   
                   break;
               case 1:
                   System.out.println("Has seleccionado la opcion 1");
                    ultDiezPost();

                   break;
                case 2:
                   System.out.println("Has seleccionado la opcion 2");
                    todosPost();

                   break;
                case 3:
                   System.out.println("Has seleccionado la opcion 3");
                   FiltrarTitutloOPalabra();

                   break;
                case 4:
                   System.out.println("Has seleccionado la opcion 4");
                   FiltrarCategoria();
                   break;

                case 5:
                   salir=true;
                   break;
                   
                default:
                   System.out.println("Solo números entre 1 y 5");
           }
            
       }
       
    }catch (Exception e) {
        e.printStackTrace();
        }
     
    }

public static void importarRSS(){

        Scanner sn = new Scanner(System.in);
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            // b.modificarBBDD("delete from clientes where id=" + ret);
            //b.leerBBDD("select top 1 * from clientes order by id desc",7);
            XmlParse x =new XmlParse();
            String url="";
            System.out.println("Introducir url:");
            url=sn.nextLine();
            System.out.println(url);
            //url ="https://lenguajedemarcasybbdd.wordpress.com/feed/";
            List<String> listtitulos = x.leer("rss/channel/item/title/text()",url);
            List<String> listcreador = x.leer("rss/channel/item/creator/text()",url);
            List<String> listfechas = x.leer("rss/channel/item/pubDate/text()",url);
            List<String> listcategoria = x.leer("rss/channel/item/category/text()",url);
            List<String> listdescripcion = x.leer("rss/channel/item/description/text()",url);
    
            for(int i=0; i<listtitulos.size();i++) {
                String consultaAutores="Select count(1) from USUARIO where nombre='"+listcreador.get(i)+"'";
                if (b.leerBBDDUnDato(consultaAutores).equals("0")){
                    String insertAutores="INSERT INTO USUARIO (Nombre) VALUES ('"+listcreador.get(i)+"')";
                    b.modificarBBDD(insertAutores);
                    System.out.println(listcreador.get(i));
                }
    
                String consultaCategorias="Select count(1) from CATEGORIA where nombre='"+listcategoria.get(i)+"'";
                if (b.leerBBDDUnDato(consultaCategorias).equals("0")){
                    String insertCategorias="INSERT INTO CATEGORIA (Nombre) VALUES ('"+listcategoria.get(i)+"')";
                    b.modificarBBDD(insertCategorias);
                    System.out.println(listcategoria.get(i));
                }
    
                String selectUsuario="Select Id from USUARIO where Nombre='"+listcreador.get(i)+"'";
    
                String selectCategoria="Select Id from CATEGORIA where Nombre='"+listcategoria.get(i)+"'";
                
                String insertTitulos="INSERT INTO POST (ID_Usuario, Titulo, Fecha, Contenido, Id_Categoria) VALUES ("+b.leerBBDDUnDato(selectUsuario)+",'"+listtitulos.get(i)+"', '"+listfechas.get(i)+"','"+listdescripcion.get(i)+"', "+b.leerBBDDUnDato(selectCategoria)+")";
                System.out.println(insertTitulos);
                b.modificarBBDD(insertTitulos);
                System.out.println(listtitulos.get(i));
                System.out.println(listfechas.get(i));
                System.out.println(listdescripcion.get(i));
            }
        }


        public static void ultDiezPost() {
            bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
                String consulta1="select top 10 Id, Titulo from post order by Id"; 
                System.out.println(b.leerBBDD(consulta1, 2));
            }


        public static void todosPost() {
            bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String consulta2="select post.Id, Titulo, Fecha, USUARIO.Nombre, CATEGORIA.Nombre  from post inner join USUARIO on post.Id_Usuario=USUARIO.Id inner join CATEGORIA on POST.Id_Categoria=CATEGORIA.Id"; 
            System.out.println(b.leerBBDD(consulta2, 5));
            }

        
        

        public static void FiltrarTitutloOPalabra() {
            bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            Scanner sn=new Scanner(System.in);
            
            System.out.println("Introducir palabra por la que quiere filtrar el titulo:");
            String filtro=sn.nextLine();
            
            String consulta3="select titulo, Contenido from POST where Titulo like '%"+filtro+"%' or Contenido like'%"+filtro+"%'";
            System.out.println(b.leerBBDD(consulta3, 2));
            }

        
            public static void FiltrarCategoria() {
                bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
                Scanner sn=new Scanner(System.in);
            
                System.out.println("Introducir palabra por la que quiere filtrar la categoria:");
                String filtro=sn.nextLine();
            
                String consulta4="select nombre from CATEGORIA where Nombre like '%"+filtro+"%'"; 
                System.out.println(b.leerBBDD(consulta4, 1));
                }
    
        
    
}
